# Install all package dependencies for Xilinx tools
FROM debian

# Set Time Zone
ENV TZ="America/Los_Angeles"
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install tzdata
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN dpkg-reconfigure --frontend noninteractive tzdata

# Update and install necessary packages
RUN apt-get update --yes && apt-get dist-upgrade --yes
RUN apt-get install -y wget unzip
RUN apt-get install -y libusb-1.0-0 udev libncurses5 libpython2.7 libwebkit2gtk-4.0-37

# Make the install directory
RUN mkdir -p /opt/st/stm32cubeide_1.7.0

#RUN wget --quiet https://www.st.com/content/ccc/resource/technical/software/sw_development_suite/group0/d5/f6/71/a8/3f/91/40/97/stm32cubeide_lnx/files/st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh.zip/jcr:content/translations/en.st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh.zip
#RUN unzip en.st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh.zip
#RUN rm en.st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh.zip
#RUN sh st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh --noexec --target .
#RUN rm st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.sh
#RUN mv st-stm32cubeide_1.7.0_10852_20210715_0634_amd64/st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.tar.gz /opt/st/stm32cubeide_1.7.0

COPY st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.tar.gz /opt/st/stm32cubeide_1.7.0
WORKDIR /opt/st/stm32cubeide_1.7.0
RUN tar xfz st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.tar.gz
RUN rm st-stm32cubeide_1.7.0_10852_20210715_0634_amd64.tar.gz